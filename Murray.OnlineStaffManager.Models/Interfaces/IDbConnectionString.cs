﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Murray.OnlineStaffManager.Models.Interfaces
{
    public interface IDbConnectionString
    {
        string GetConnectionString();
    }

    public class ConnectionString : IDbConnectionString
    {
        public string GetConnectionString()
        {
            //todo: move connection string to config file
            return "Data Source=localhost; Database=OnlineStaffManager; Integrated Security=true;";
        }
    }
}

