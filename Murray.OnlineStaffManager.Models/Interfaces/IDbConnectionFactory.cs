﻿using System.Data;

namespace Murray.OnlineStaffManager.Models.Interfaces
{
    public interface IDbConnectionFactory
    {
        IDbConnection CreateConnection();
        string GetConnectionString();
    }
}
