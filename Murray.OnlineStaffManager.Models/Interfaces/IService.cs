﻿using System;
using System.Collections.Generic;

namespace Murray.OnlineStaffManager.Models.Interfaces
{
    public interface IService<T> where T : class
    {
        /// <summary>
        /// Adds an entity to the database
        /// </summary>
        /// <param name="entity">The entity that is to be added</param>
        /// <returns>True if successfull; else false.</returns>
        bool Add(T entity);
        /// <summary>
        /// Removes an entity from the database
        /// </summary>
        /// <param name="entity">The entity to be removed</param>
        /// <returns>True if removed succesfully; else false.</returns>
        bool Delete(T entity);
        /// <summary>
        /// Updates an entity in the database.
        /// </summary>
        /// <param name="entity">The entity to be updated</param>
        /// <returns>True if update was succesful; else false.</returns>
        bool Update(T entity);
        /// <summary>
        /// Gets all objects of type T.
        /// </summary>
        /// <returns>An enumeration of all the objects of type T from the database</returns>
        IEnumerable<T> GetAll(Guid CompanyId);
        /// <summary>
        /// Gets a specific object from the database by id.
        /// </summary>
        /// <param name="id">The id of the entity that is to be searched for and returned</param>
        /// <returns>The entity that has been searched for if successful; else empty an entity.</returns>
        T Get(Guid id);
        /// <summary>
        /// Deletes an entity from the database by id.
        /// </summary>
        /// <param name="id">id of the entity that is to be deleted</param>
        /// <returns>True if removed successfully; else false.</returns>
        bool DeleteById(Guid id);
    }
}
