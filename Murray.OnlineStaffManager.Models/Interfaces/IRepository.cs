﻿using System;
using System.Linq;

namespace Murray.OnlineStaffManager.Models.Interfaces
{
    public interface IRepository<T> where T : class
    {
        bool Add(T entity);
        bool Delete(T entity);
        bool DeleteById(Guid id);
        bool Update(T entity);
        T GetById(Guid id);
        IQueryable<T> All(Guid CompanyId);
    }
}
