﻿using System;

namespace Murray.OnlineStaffManager.Models
{
    public class Company : BaseModel
    {
        public string Name { get; set; }
        public string WebsiteUrl { get; set; }
    }
}
