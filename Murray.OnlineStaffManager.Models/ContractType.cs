﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Murray.OnlineStaffManager.Models
{
    public class ContractType : BaseModel
    {
        public string Description { get; set; }
    }
}
