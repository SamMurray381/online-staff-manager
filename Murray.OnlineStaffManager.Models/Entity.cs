﻿using System;

namespace Murray.OnlineStaffManager.Models
{
    /// <summary>
    /// Used for deleting entities via webapi AJAX calls.
    /// </summary>
    public class Entity
    {
        public string Id { get; set; }

        public Guid IdAsGuid { get { return Guid.Parse(this.Id); } }
    }
}
