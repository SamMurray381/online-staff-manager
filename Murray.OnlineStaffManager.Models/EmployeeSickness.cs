﻿using System;

namespace Murray.OnlineStaffManager.Models
{
    public class EmployeeSickness : BaseModel
    {
        public Guid EmployeeId { get; set; }
        public string Forename { get; set; }
        public string Surname { get; set; }
        public DateTime SicknessStartDate { get; set; }
        public DateTime? SicknessEndDate { get; set; }
        public Guid SicknessTypeId { get; set; }
        public string Comments { get; set; }
        public bool Deleted { get; set; }
        public Guid CompanyId { get; set; }
    }
}
