﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Murray.OnlineStaffManager.Models
{
    public class BaseModel
    {
        public BaseModel()
        {
            CreatedOn = DateTime.Now;
        }

        /// <summary>
        /// Unique global identifier of an entity.
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// Date that the entity was created.
        /// </summary>
        public DateTime CreatedOn { get; set; }
    }
}
