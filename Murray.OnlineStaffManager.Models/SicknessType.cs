﻿using System;

namespace Murray.OnlineStaffManager.Models
{
    public class SicknessType : BaseModel
    {
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public Guid CompanyId { get; set; }
    }
}
