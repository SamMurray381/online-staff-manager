﻿using System;

namespace Murray.OnlineStaffManager.Models
{
    /// <summary>
    /// Represents an 'AppMember' without all of the bloat: e.g. PasswordHash, SecurityStamp.
    /// </summary>
    public class AppUser 
    {
        public Guid Id { get; set; }
        public Guid CompanyId { get; set; }
        public string Forename { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
    }
}
