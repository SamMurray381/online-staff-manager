﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Murray.OnlineStaffManager.Models
{
    public class Employee : BaseModel
    {
        /// <summary>
        /// Forename of the Employee.
        /// </summary>
        public string Forename { get; set; }
        /// <summary>
        /// Surname of the Employee.
        /// </summary>
        public string Surname { get; set; }
        /// <summary>
        /// Date of birth for the the Employee.
        /// </summary>
        public DateTime DateOfBirth { get; set; }
        /// <summary>
        /// Day that the Employee began to work for the company.
        /// </summary>
        public DateTime EmployeeStartDate { get; set; }
        public DateTime EmployeeEndDate { get; set; }
        public bool IsActive { get; set; }
        public Guid CompanyId { get; set; }
        [NotMapped]
        public string FullName { get { return this.Forename + " " + this.Surname; } }
    }
}
