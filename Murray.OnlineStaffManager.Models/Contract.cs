﻿using System;

namespace Murray.OnlineStaffManager.Models
{
    /// <summary>
    /// Represents an employment Contract between an Employee and the Company  
    /// </summary>
    public class Contract : BaseModel
    {
        public Guid EmployeeId { get; set; }
        public string ContractInformation { get; set; }
        public DateTime ContractStartDate { get; set; }
        public DateTime? ContractEndDate { get; set; }
        public int ContractTypeId { get; set; }
    }
}
