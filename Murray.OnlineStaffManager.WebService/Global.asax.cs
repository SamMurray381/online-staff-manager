﻿using Murray.OnlineStaffManager.Core;
using Murray.OnlineStaffManager.DAL.Repositories;
using Murray.OnlineStaffManager.DAL.Services;
using Murray.OnlineStaffManager.Models.Interfaces;
using SimpleInjector;
using SimpleInjector.Integration.WebApi;
using System.Net.Http.Formatting;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Murray.OnlineStaffManager.WebService
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            GlobalConfiguration.Configuration.Formatters.Clear();

            GlobalConfiguration.Configuration.Formatters.Clear();
            // Force JSON responses
            GlobalConfiguration.Configuration.Formatters.Add(new JsonMediaTypeFormatter());
        }
    }
}
