﻿using Murray.OnlineStaffManager.DAL.Repositories;
using Murray.OnlineStaffManager.Models;
using System.Web.Http;

namespace Murray.OnlineStaffManager.WebService.Controllers
{
    [Authorize]
    public class AppMemberController : ApiController
    {
        IAppMemberRepository _appMemberRepository;

        public AppMemberController(IAppMemberRepository appMemberRepository)
        {
            _appMemberRepository = appMemberRepository;
        }

        [HttpPut]
        public IHttpActionResult Put(AppUser appUser)
        {
            bool isSuccess = _appMemberRepository.Update(appUser);
            if (isSuccess)
            {
                return Ok(appUser.Id);
            }
            return BadRequest(appUser.Id.ToString());
        }
    }
}