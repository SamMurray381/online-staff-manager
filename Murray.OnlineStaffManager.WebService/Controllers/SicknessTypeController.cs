﻿using Murray.OnlineStaffManager.DAL.Services;
using Murray.OnlineStaffManager.Models;
using System.Web.Http;

namespace Murray.OnlineStaffManager.WebService.Controllers
{
    [Authorize]
    public class SicknessTypeController : ApiController
    {
        private IEmployeeSicknessService _employeeSicknessService;
        public SicknessTypeController(IEmployeeSicknessService employeeSicknessService)
        {
            _employeeSicknessService = employeeSicknessService;
        }
        [HttpDelete]
        public IHttpActionResult Delete(Entity model)
        {
            return Ok(_employeeSicknessService.DeleteSicknessTypeById(model.IdAsGuid));
        }
    }
}