﻿using Murray.OnlineStaffManager.DAL.Services;
using Murray.OnlineStaffManager.Models;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace Murray.OnlineStaffManager.WebService.Controllers
{
    [Authorize]
    public class SicknessController : ApiController
    {
        private readonly IEmployeeSicknessService _employeeSicknessService;

        public SicknessController(IEmployeeSicknessService employeeSicknessService)
        {
            _employeeSicknessService = employeeSicknessService;
        }

        [HttpGet]
        public EmployeeSickness GetById(Guid id)
        {
            return _employeeSicknessService.Get(id);
        }

        [HttpGet]
        public IEnumerable<EmployeeSickness> GetAll()
        {
            throw new NotImplementedException();
        }

        [HttpPost]
        public bool Post(EmployeeSickness entity)
        {
            return _employeeSicknessService.Add(entity);
        }

        [HttpPut]
        public bool Put(EmployeeSickness entity)
        {
            return _employeeSicknessService.Update(entity);
        }

        [HttpDelete]
        public bool Delete(Entity entity)
        {
            return _employeeSicknessService.DeleteById(entity.IdAsGuid);
        }
    }
}