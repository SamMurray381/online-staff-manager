﻿using Murray.OnlineStaffManager.DAL.Repositories;
using Murray.OnlineStaffManager.Models;
using System.Web.Http;

namespace Murray.OnlineStaffManager.WebService.Controllers
{
    [Authorize]
    public class CompanyController : ApiController
    {
        private ICompanyRepository _companyRepository;

        public CompanyController(ICompanyRepository companyRepository)
        {
            _companyRepository = companyRepository;
        }

        [HttpPut]
        public IHttpActionResult Put(Company company)
        {
            bool isSuccess = _companyRepository.Update(company);
            if (isSuccess)
            {
                return Ok(company.Id);
            }
            else
            {
                return BadRequest(company.Id.ToString());
            }
        }
    }
}