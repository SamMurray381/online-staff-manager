﻿using Murray.OnlineStaffManager.DAL.Services;
using Murray.OnlineStaffManager.Models;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace Murray.OnlineStaffManager.WebService.Controllers
{
    [Authorize]
    public class EmployeeController : ApiController
    {
        private readonly IEmployeeService _employeeService;

        public EmployeeController(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }

        [HttpGet]
        [Route("All")]
        public IEnumerable<Employee> GetAll(Guid CompanyId)
        {
            return _employeeService.GetAll(CompanyId);
        }

        [HttpGet]
        public Employee Get(Guid id)
        {
            return _employeeService.Get(id);
        }

        [HttpPost]
        public void Post(Employee entity)
        {
            _employeeService.Add(entity);
        }

        [HttpPut]
        public IHttpActionResult Put(Employee entity)
        {
            _employeeService.Update(entity);

            return Ok(entity);
        }
        [HttpDelete]
        public IHttpActionResult Delete(Entity entity)
        {
            Guid idAsGuid; 
            if(Guid.TryParse(entity.Id, out idAsGuid)){
                bool isSuccess = _employeeService.DeleteById(idAsGuid);
                if (isSuccess)
                {
                    return Ok(idAsGuid);
                }
            }
            return BadRequest();
        }
    }
}
