﻿using Murray.OnlineStaffManager.DAL.Services;
using System;
using System.Linq;
using System.Web.Http;

namespace Murray.OnlineStaffManager.WebService.Controllers
{
    [Authorize]
    public class StatsController : ApiController
    {
        private readonly IEmployeeService _employeeService;

        public StatsController(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }

        [HttpGet]
        public int Get(Guid companyId)
        {
            return _employeeService.GetAll(companyId)
                .Where(m => m.CompanyId == companyId && m.IsActive)
                .Count();
        }
    }
}
