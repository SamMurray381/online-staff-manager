﻿using System.Web.Mvc;

namespace Murray.OnlineStaffManager.WebService.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }
    }
}
