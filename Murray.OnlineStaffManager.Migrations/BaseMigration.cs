﻿using System;
using FluentMigrator;

namespace Murray.OnlineStaffManager.Migrations
{
    [DatabaseMigration(year: 2016, day:17, month: 12, hour: 13, minute: 10)]
    public class BaseMigration : Migration
    {
        public override void Up()
        {
            // Create Employee table
            Create.Table("Employee")
                .WithIdColumnGuid("Id")
                .WithColumn("Forename")
                .AsString()
                .WithColumn("Surname")
                .AsString()
                .WithColumn("DateOfBirth")
                .AsDateTime()
                .WithDefaultValue(DateTime.MinValue)
                .WithColumn("EmployeeStartDate")
                .AsDateTime()
                .WithDefaultValue(DateTime.MinValue)
                .WithColumn("IsActive")
                .AsBoolean()
                .NotNullable()
                .WithDefaultValue(true)
                .WithColumn("CompanyId")
                .AsGuid()
                .NotNullable(); 
        }

        public override void Down()
        {
            // Do nothing
        }
    }
}
