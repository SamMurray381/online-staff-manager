﻿using FluentMigrator;

namespace Murray.OnlineStaffManager.Migrations
{
    public class DatabaseMigration : MigrationAttribute
    {
        public DatabaseMigration(int year, int month, int day, int hour, int minute)
           : base(calculateValue(year, month, day, hour, minute))
        {
        }

        private static long calculateValue(int year, int month, int day, int hour, int minute)
        {
            return year * 100000000L + month * 1000000L + day * 10000L + hour * 100L + minute;
        }
    }
}
