﻿using FluentMigrator.Builders.Create.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Murray.OnlineStaffManager.Migrations
{
    /// <summary>
    /// Customised extensions that we create ourself to reduce code bloat.
    /// </summary>
    internal static class MigrationExtensions
    {
        /// <summary>
        /// With this extension we can now create an identity column by simply doing: Create.Table("Users").WithIdColumn();
        /// Instead of: .WithColumn("Id").AsInt32().NotNullable().PrimaryKey().Identity();
        /// This improves readability for us developers.
        /// </summary>
        /// <param name="tableWithColumnSyntax"></param>
        public static ICreateTableColumnOptionOrWithColumnSyntax WithIdColumn(this ICreateTableWithColumnSyntax tableWithColumnSyntax, string name)
        {
            return tableWithColumnSyntax
                .WithColumn(name)
                .AsGuid()
                .NotNullable()
                .PrimaryKey();
        }

        /// <summary>
        /// With this extension we can now create an identity column by simply doing: Create.Table("Users").WithIdColumn();
        /// Instead of: .WithColumn("Id").AsInt32().NotNullable().PrimaryKey().Identity();
        /// This improves readability for us developers.
        /// </summary>
        /// <param name="tableWithColumnSyntax"></param>
        public static ICreateTableColumnOptionOrWithColumnSyntax WithIdColumnInt(this ICreateTableWithColumnSyntax tableWithColumnSyntax, string name)
        {
            return tableWithColumnSyntax
                .WithColumn(name)
                .AsInt32()
                .NotNullable()
                .PrimaryKey()
                .Identity();
        }

        /// <summary>
        /// With this extension we can now create an identity column by simply doing: Create.Table("Users").WithIdColumn();
        /// Instead of: .WithColumn("Id").AsInt32().NotNullable().PrimaryKey().Identity();
        /// This improves readability for us developers.
        /// </summary>
        /// <param name="tableWithColumnSyntax"></param>
        public static ICreateTableColumnOptionOrWithColumnSyntax WithIdColumnGuid(this ICreateTableWithColumnSyntax tableWithColumnSyntax, string name)
        {
            return tableWithColumnSyntax
                .WithColumn(name)
                .AsGuid()
                .NotNullable()
                .PrimaryKey();
        }

        /// <summary>
        /// As above, we can easily create a table that has columns for timestamps by doing: Create.Table("Users").WithTimeStamps();
        /// This reduces the amount of code we have to write to get the same outcome.
        /// </summary>
        /// <param name="tableWithColumnSyntax"></param>
        public static ICreateTableColumnOptionOrWithColumnSyntax WithTimeStamps(this ICreateTableWithColumnSyntax tableWithColumnSyntax)
        {
            return tableWithColumnSyntax
                .WithColumn("CreatedAt").AsDateTime().NotNullable()
                .WithColumn("ModifiedAt").AsDateTime().NotNullable();
        }
    }
}
