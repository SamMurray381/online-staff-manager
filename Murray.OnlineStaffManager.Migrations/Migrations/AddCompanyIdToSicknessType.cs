﻿using FluentMigrator;

namespace Murray.OnlineStaffManager.Migrations
{
    [DatabaseMigration(year: 2016, day: 17, month: 12, hour: 22, minute: 25)]
    public class AddCompanyIdToSicknessType : Migration
    {
        public override void Up() 
        {
            Create.Column("CompanyId")
                .OnTable("SicknessType")
                .AsGuid()
                .NotNullable();
        }

        public override void Down()
        {
            Delete.Column("CompanyId")
                .FromTable("SicknessType");
        }
    }
}
