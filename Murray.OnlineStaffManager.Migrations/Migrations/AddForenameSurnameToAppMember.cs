﻿using FluentMigrator;

namespace Murray.OnlineStaffManager.Migrations
{
    [DatabaseMigration(year: 2016, day: 17, month: 12, hour: 13, minute: 15)]
    public class AddForenameSurnameToAppMember : Migration
    {
        public override void Up()
        {
            Create.Column("Forename")
               .OnTable("Member")
               .AsString()
               .WithDefaultValue("");

            Create.Column("Surname")
                .OnTable("Member")
                .AsString()
                .WithDefaultValue("");
        }

        public override void Down()
        {
            Delete.Column("Forename")
                .FromTable("Member");

            Delete.Column("Surname")
                .FromTable("Member");
        }
    }
}
