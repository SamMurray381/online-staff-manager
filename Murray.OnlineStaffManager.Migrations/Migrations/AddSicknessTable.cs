﻿using FluentMigrator;
using System;

namespace Murray.OnlineStaffManager.Migrations
{
    [DatabaseMigration(year: 2016, day: 17, month: 12, hour: 13, minute: 12)]
    public class AddSicknessTable : Migration
    {
        public override void Up()
        {
            Create.Table("EmployeeSickness")
                .WithIdColumnGuid("Id")
                .WithColumn("EmployeeId")
                .AsGuid()
                .NotNullable()
                .WithColumn("SicknessStartDate")
                .AsDate()
                .NotNullable()
                .WithColumn("SicknessEndDate")
                .AsDate()
                .Nullable()
                .WithColumn("SicknessTypeId")
                .AsGuid()
                .NotNullable()
                .WithColumn("Comments")
                .AsString()
                .Nullable();

            // Create a sickness type lookup.
            Create.Table("SicknessType")
                .WithIdColumnGuid("Id")
                .WithColumn("Description")
                .AsString()
                .NotNullable();

            // Create a relationship between the Sickness and Employee tables
            Create.ForeignKey("FK_Sickness_EmployeeId_Employee_Id").FromTable("EmployeeSickness")
                 .ForeignColumn("EmployeeId")
                 .ToTable("Employee")
                 .PrimaryColumn("Id");

            // Create a relationship between the Sickness and SicknessType tables
            Create.ForeignKey("FK_Sickness_SicknessTypeId_SicknessType_Id").FromTable("EmployeeSickness")
                 .ForeignColumn("SicknessTypeId")
                 .ToTable("SicknessType")
                 .PrimaryColumn("Id");
        }

        public override void Down()
        {
            // Remove constraints
            Delete.ForeignKey("FK_Sickness_EmployeeId_Employee_EmployeeId").OnTable("Sickness");
            Delete.ForeignKey("FK_Sickness_SicknessTypeId_SicknessType_SicknessTypeId").OnTable("Sickness");

            // Delete tables
            Delete.Table("EmployeeSickness");
            Delete.Table("SicknessType");
        }
    }
}
