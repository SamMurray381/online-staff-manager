﻿using FluentMigrator;

namespace Murray.OnlineStaffManager.Migrations
{
    [DatabaseMigration(year: 2016, day: 17, month: 12, hour: 13, minute: 16)]
    public class AddEndDateToEmployee : Migration
    {
        public override void Up()
        {
            Create.Column("EmployeeEndDate")
                .OnTable("Employee")
                .AsDateTime()
                .Nullable()
                .WithDefaultValue(null);
        }

        public override void Down()
        {
            Delete.Column("EmployeeEndDate")
                .FromTable("Employee");
        }
    }
}
