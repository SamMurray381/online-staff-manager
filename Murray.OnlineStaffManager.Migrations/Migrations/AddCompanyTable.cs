﻿using FluentMigrator;
using System;

namespace Murray.OnlineStaffManager.Migrations.Migrations
{
    [DatabaseMigration(year: 2016, month: 12, day: 17, hour: 14, minute: 47)]
    public class AddCompanyTable : Migration
    {
        public override void Up()
        {
            // Add Company table and fk to Member
            Create.Table("Company")
                .WithIdColumnGuid("Id")
                .WithColumn("Name")
                .AsAnsiString()
                .NotNullable()
                .WithColumn("WebsiteUrl")
                .AsString()
                .Nullable();

            Create.Column("CompanyId")
                .OnTable("Member")
                .AsGuid()
                .NotNullable();

            Create.ForeignKey("FK_Company_Id_Member_Id")
              .FromTable("Member")
              .ForeignColumn("CompanyId")
              .ToTable("Company")
              .PrimaryColumn("Id");
        }

        public override void Down()
        {
            Delete.ForeignKey("FK_Company_Id_Member_Id");
            Delete.Table("Company");
        }
    }
}
