﻿using FluentMigrator;

namespace Murray.OnlineStaffManager.Migrations
{
    [DatabaseMigration(year: 2016, day: 19, month: 12, hour: 14, minute: 32)]
    public class AddCreatedOnToCompanyTable : Migration
    {
        public override void Up() 
        {
            Create.Column("CreatedOn")
                .OnTable("Company")
                .AsDateTime()
                .NotNullable()
                .WithDefault(SystemMethods.CurrentDateTime);
        }

        public override void Down()
        {
            Delete.Column("CreatedOn")
                .FromTable("Company");
        }
    }
}
