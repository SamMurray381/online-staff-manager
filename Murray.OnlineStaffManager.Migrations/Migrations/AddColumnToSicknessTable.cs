﻿using FluentMigrator;

namespace Murray.OnlineStaffManager.Migrations
{
    [DatabaseMigration(year: 2016, day: 17, month: 12, hour: 13, minute: 13)]
    public class AddColumnToSicknessTable : Migration
    {
        public override void Up() 
        {
            Create.Column("Deleted")
                .OnTable("EmployeeSickness")
                .AsBoolean()
                .WithDefaultValue(false);
        }

        public override void Down()
        {
            Delete.Column("Deleted")
                .FromTable("EmployeeSickness");
        }
    }
}
