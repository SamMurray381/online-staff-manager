﻿using FluentMigrator;

namespace Murray.OnlineStaffManager.Migrations
{
    [DatabaseMigration(year: 2016, day: 17, month: 12, hour: 13, minute: 22)]
    public class UpdateSickenssTypeTable : Migration
    {
        public override void Up()
        {
            Create.Column("IsActive")
                .OnTable("SicknessType")
                .AsBoolean()
                .WithDefaultValue(true);

            Create.Column("CreatedOn")
                .OnTable("SicknessType")
                .AsDateTime()
                .WithDefault(SystemMethods.CurrentDateTime);
        }

        public override void Down()
        {
            Delete.Column("IsActive")
                .FromTable("SicknessType");

            Delete.Column("CreatedOn")
                .FromTable("SicknessType");
        }
    }
}
