# Employee Smart #

I created this repository for my own self learning of new technologies. The application is an employee management web application that allows people to track employees in their company. I'm constantly updating this repository to add new features and technologies. Anyone is free to fork and use this code base for personal non-commercial use. 

### Running Employee Smart Locally ###

* First of all, clone the source code to your dev machine. 
* Ensure that you have a local instance of SQL server and create a database called 'OnlineStaffManager' (or change the connection string / catalogue name to match your setup).
* Open the Visual Studio solution which is located in the root.
* Create a config file in the root of the WebService project and name it AppSecrets,config and place two two keys, one for AudienceId and one for AudienceSecret.
* Run the Murray.OnlineStaffManager.Migrations project - this will apply the database schema. If you have a non-standard setup make sure to change the connection string.
* Set Murray.OnlineStaffManager.Web and Murray.OnlineStaffManager.WebService as start up projects and run.
* All done!