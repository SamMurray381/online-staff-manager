﻿using Murray.OnlineStaffManager.Models.Interfaces;
using System.Data;
using System.Data.SqlClient;

namespace Murray.OnlineStaffManager.Core
{
    public class SqlConnectionFactory : IDbConnectionFactory
    {
        //todo: move connection string to config file
        private string CONNECTION_STRING = "Data Source=localhost; Database=OnlineStaffManager; Integrated Security=true;";

        public IDbConnection CreateConnection()
        {
            var conn = new SqlConnection(CONNECTION_STRING);
            return conn;
        }

        public string GetConnectionString()
        {
            return CONNECTION_STRING;
        }
    }
}
