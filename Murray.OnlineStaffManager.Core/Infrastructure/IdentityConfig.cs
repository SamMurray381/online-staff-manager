﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Murray.OnlineStaffManager.Models.Interfaces;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Configuration;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Murray.OnlineStaffManager.Core.Infrastructure
{
    public class IdentityConfig
    {
        public class EmailService : IIdentityMessageService
        {
            public async Task SendAsync(IdentityMessage message)
            {
                await ConfigSendGridasync(message);
            }

            private async Task ConfigSendGridasync(IdentityMessage message)
            {
                string apiKey = ConfigurationManager.AppSettings["as:SendGridApiKey"]; ;
                dynamic sg = new SendGridAPIClient(apiKey, "https://api.sendgrid.com");

                Email from = new Email("donotreply@employee-smart.uk", "Employee Smart");
                string subject = message.Subject;
                Email to = new Email(message.Destination);
                Content content = new Content("text/html", message.Body);

                try
                {
                    Mail mail = new Mail(from, subject, to, content);
                    dynamic response =
                        await sg.client.mail.send.post(requestBody: mail.Get());
                }
                catch (Exception ex)
                {
                    throw ex;
                    //todo: add an error logging service to log errors to database.
                }
            }
        }

        public class SmsService : IIdentityMessageService
        {
            public Task SendAsync(IdentityMessage message)
            {
                // Plug in your SMS service here to send a text message.
                return Task.FromResult(0);
            }
        }

        // Configure the application AppMember manager used in this application. UserManager is defined in ASP.NET Identity and is used by the application.
        public class ApplicationUserManager : UserManager<AppMember, Guid>
        {
            IDbConnectionFactory _dbConnectionFactory;

            public ApplicationUserManager(
                IDbConnectionFactory dbConnectionFactory, 
                IDbConnectionString conn)
                : base(new UserStore<AppMember>(
                    new DbManager(conn)))
            {
                _dbConnectionFactory = dbConnectionFactory;
            }

            public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context)
            {
                IDbConnectionFactory sqlConnectionFactory = new SqlConnectionFactory();
                IDbConnectionString dbConnectionString = new ConnectionString();

                var manager = new ApplicationUserManager
                    (sqlConnectionFactory, dbConnectionString);

                // Configure validation logic for usernames
                manager.UserValidator = new UserValidator<AppMember, Guid>(manager)
                {
                    AllowOnlyAlphanumericUserNames = false,
                    RequireUniqueEmail = true
                };

                // Configure validation logic for passwords
                manager.PasswordValidator = new PasswordValidator
                {
                    RequiredLength = 6,
                    RequireNonLetterOrDigit = false,
                    RequireDigit = false,
                    RequireLowercase = false,
                    RequireUppercase = false,
                };

                // Configure AppMember lockout defaults
                manager.UserLockoutEnabledByDefault = false;
                manager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
                manager.MaxFailedAccessAttemptsBeforeLockout = 5;
                // Register two factor authentication providers. This application uses Phone and Emails as a step of receiving a code for verifying the AppMember
                // You can write your own provider and plug it in here.
                manager.RegisterTwoFactorProvider("Phone Code", new PhoneNumberTokenProvider<AppMember, Guid>
                {
                    MessageFormat = "Your security code is {0}"
                });
                manager.RegisterTwoFactorProvider("Email Code", new EmailTokenProvider<AppMember, Guid>
                {
                    Subject = "Security Code",
                    BodyFormat = "Your security code is {0}"
                });
                manager.EmailService = new EmailService();
                manager.SmsService = new SmsService();
                if(options != null)
                {
                    var dataProtectionProvider = options.DataProtectionProvider;
                    if (dataProtectionProvider != null)
                    {
                        manager.UserTokenProvider =
                            new DataProtectorTokenProvider<AppMember, Guid>
                            (dataProtectionProvider.Create("ASP.NET Identity"));
                    }
                }
                return manager;
            }
        }

        // Configure the application sign-in manager which is used in this application.
        public class ApplicationSignInManager : SignInManager<AppMember, Guid>
        {
            public ApplicationSignInManager(ApplicationUserManager userManager, IAuthenticationManager authenticationManager)
                : base(userManager, authenticationManager)
            {
            }

            public override Task<ClaimsIdentity> CreateUserIdentityAsync(AppMember AppMember)
            {
                return AppMember.GenerateUserIdentityAsync((ApplicationUserManager)UserManager);
            }

            public static ApplicationSignInManager Create(IdentityFactoryOptions<ApplicationSignInManager> options, IOwinContext context)
            {
                return new ApplicationSignInManager(context.GetUserManager<ApplicationUserManager>(), context.Authentication);
            }
        }
    }
}
