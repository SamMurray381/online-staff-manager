﻿using Microsoft.AspNet.Identity;
using Murray.OnlineStaffManager.Models.Interfaces;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Murray.OnlineStaffManager.Core.Infrastructure
{
    public class AppMember : IdentityMember
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<AppMember, Guid> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom AppMember claims here
            return userIdentity;
        }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<AppMember, Guid> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom AppMember claims here
            return userIdentity;
        }
    }

    /// <summary>
    /// Create and opens a connection to a MSSql database
    /// </summary>
    public class ApplicationDbContext : DbManager
    {
        public ApplicationDbContext(IDbConnectionString connectionString)
            : base(connectionString)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext(new ConnectionString());
        }
    }
}
