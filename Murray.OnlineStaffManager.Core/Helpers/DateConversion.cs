﻿using System;
using System.Globalization;

namespace Murray.OnlineStaffManager.Core.Helpers
{
    public static class DateConversion
    {
        public static string ConvertUKDateToSqlDate(DateTime UkDate)
        {
            return UkDate.ToString("yyyy-MM-dd HH:mm:ss");
        }
    }
}
