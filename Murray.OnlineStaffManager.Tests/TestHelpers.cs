﻿using Murray.OnlineStaffManager.Models;
using System;

namespace Murray.OnlineStaffManager.Tests
{
    public static class TestHelpers
    {
        /// <summary>
        /// Helper method to create a new unique employee using DateTime.Now.Ticks
        /// </summary>
        /// <returns>An Employee with a unique first name and EmployeeId</returns>
        public static Employee CreateEmployee()
        {
            Employee employee = new Employee
            {
                Id = Guid.NewGuid(),
                Forename = "Unit-Test-User-" + DateTime.Now.Ticks,
                Surname = "Unit-Test-Surname",
                EmployeeStartDate = DateTime.Now,
                DateOfBirth = DateTime.Now
            };

            return employee;
        }

        public static Contract CreateContract(Guid employeeId)
        {
            Contract contract = new Contract
            {
                Id = Guid.NewGuid(),
                ContractEndDate = DateTime.Now.AddYears(2),
                ContractStartDate = DateTime.Now,
                ContractTypeId = 1,
                ContractInformation = "This employee has only recently been put on this contract",
                EmployeeId = employeeId
            };

            return contract;
        }

        public static EmployeeSickness CreateEmployeeSickness(Guid employeeId)
        {
            // Get a sickness Id from database.

            EmployeeSickness employeeSickness = new EmployeeSickness
            {
                Id = Guid.NewGuid(),
                EmployeeId = employeeId,
                SicknessStartDate = DateTime.Now,
                SicknessEndDate = null,
                SicknessTypeId = new Guid(), // dummy for now...
                Comments = "This is a test sickness"
            };

            return employeeSickness;
        }

        /// <summary>
        /// Creates an Employee and assigns them a standard Contract. 
        /// </summary>
        /// <returns>An Employee with a contract</returns>
        public static Employee CreateEmployeeWithStandardContract()
        {
            Employee employee = CreateEmployee();

            Contract contract = new Contract
            {
                Id = Guid.NewGuid(),
                ContractEndDate = DateTime.Now.AddYears(2),
                ContractStartDate = DateTime.Now,
                ContractTypeId = 1,
                ContractInformation = "This employee has only recently been put on this contract",
                EmployeeId = employee.Id
            };

            return employee;
        }
    }
}
