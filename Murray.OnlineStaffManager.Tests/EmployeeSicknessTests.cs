﻿using Murray.OnlineStaffManager.DAL;
using Murray.OnlineStaffManager.Models;
using NUnit.Framework;
using System.Transactions;

namespace Murray.OnlineStaffManager.Tests
{
    public class EmployeeSicknessTests
    {
        private TransactionScope _transactionScope;

        [SetUp]
        public void Initialize()
        {
            _transactionScope = new TransactionScope();
        }

        [TearDown]
        public void CleanUp()
        {
            _transactionScope.Dispose();
        }
    }
}
