﻿using Murray.OnlineStaffManager.Core;
using Murray.OnlineStaffManager.DAL.Repositories;
using Murray.OnlineStaffManager.Models;
using NUnit.Framework;
using System.Transactions;

namespace Murray.OnlineStaffManager.Tests
{
    [TestFixture]
    public class EmployeeTests
    {
        private TransactionScope _transactionScope;

        [SetUp]
        public void Initialize()
        {
            _transactionScope = new TransactionScope();
        }

        [TearDown]
        public void CleanUp()
        {
            _transactionScope.Dispose();
        }

        /// <summary>
        /// Delete tests are faulty for now, will fix later.
        /// </summary>
        [Test]
        public void CanDeleteEmployee()
        {
            EmployeeRepository employeeRepository = new EmployeeRepository(new SqlConnectionFactory());
            Employee employee = TestHelpers.CreateEmployee();

            // Add an employee
            employeeRepository.Add(employee);

            // Now try and delete!
            employeeRepository.Delete(employee);

            // Check that the deletion was a success
            Assert.IsTrue(employeeRepository.GetById(employee.Id) == null);
        }

        [Test]
        public void CanAddEmployee()
        {
            EmployeeRepository employeeRepository = new EmployeeRepository(new SqlConnectionFactory());

            // Create an employee to add
            Employee employee = TestHelpers.CreateEmployee();

            employeeRepository.Add(employee);

            Employee addedEmployee = employeeRepository.GetById(employee.Id);

            // Check that the employee was added to the db successfully
            Assert.IsTrue(addedEmployee != null);
        }
    }
}
