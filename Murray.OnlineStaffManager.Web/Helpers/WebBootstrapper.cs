﻿using AutoMapper;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Murray.OnlineStaffManager.Core;
using Murray.OnlineStaffManager.DAL.Repositories;
using Murray.OnlineStaffManager.DAL.Services;
using Murray.OnlineStaffManager.Models;
using Murray.OnlineStaffManager.Models.Interfaces;
using Murray.OnlineStaffManager.Web.Models.ViewModels;
using SimpleInjector;
using SimpleInjector.Advanced;
using SimpleInjector.Integration.Web;
using SimpleInjector.Integration.Web.Mvc;
using System.Collections.Generic;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace Murray.OnlineStaffManager.Web.Helpers
{
    public static class WebBootstrapper
    {
        public static void BootstrapSimpleInjector()
        {
            // Create the container as usual.
            var container = new Container();
            container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();

            // Register your types, for instance:
            container.Register<IEmployeeService, EmployeeService>(Lifestyle.Transient);
            container.Register<IEmployeeRepository, EmployeeRepository>(Lifestyle.Scoped);
            container.Register<IDbConnectionFactory, SqlConnectionFactory>(Lifestyle.Scoped);
            container.Register<IEmployeeSicknessRepository, EmployeeSicknessRepository>(Lifestyle.Scoped);
            container.Register<IAppMemberRepository, AppMemberRepository>(Lifestyle.Scoped);
            container.Register<ICompanyRepository, CompanyRepository>(Lifestyle.Scoped);

            container.Register<IEmployeeSicknessService, SicknessService>(Lifestyle.Transient);
            container.Register<IDbConnectionString, ConnectionString>(Lifestyle.Scoped);

            container.Register<DbManager>(Lifestyle.Scoped);

            container.RegisterPerWebRequest<IAuthenticationManager>(() =>
            AdvancedExtensions.IsVerifying(container) ?
            new OwinContext(new Dictionary<string, object>()).Authentication :
            HttpContext.Current.GetOwinContext().Authentication);

            // This is an extension method from the integration package.
            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());

            // This is an extension method from the integration package as well.
            container.RegisterMvcIntegratedFilterProvider();

            //container.Verify();

            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
        }

        public static void BootstrapAutoMapper()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Employee, ListEmployeesViewModel>();
                cfg.CreateMap<Employee, EditEmployeeViewModel>();
                cfg.CreateMap<Employee, EmployeeAddViewModel>();
                cfg.CreateMap<EmployeeSickness, EmployeeSicknessListViewModel>();
                cfg.CreateMap<EmployeeSickness, EmployeeSicknessViewModel>();
                cfg.CreateMap<EmployeeSicknessAddViewModel, EmployeeSickness>();
                cfg.CreateMap<EmployeeSickness, EmployeeSicknessDetailViewModel>();
                cfg.CreateMap<SicknessType, SicknessTypesListViewModel>();
                cfg.CreateMap<AppUser, MemberDetailsViewModel>();
                cfg.CreateMap<Company, CompanyDetailsViewModel>();
            });
        }
    }
}