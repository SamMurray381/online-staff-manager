﻿function parseDate(date) {
    if (date === '') {
        return null;
    }

    date = date.split("/");
    var newDate = date[1] + "/" + date[0] + "/" + date[2];
    return new Date(newDate).toDateString();
}

function initDatePickers() {
    $('.input-group.date').datepicker({
        format: 'dd/mm/yyyy',
        orientation: 'bottom'
    });
}

function getApiBase() {
    return "https://api.employee-smart.uk/";
}