﻿function getToken() {
    var token = sessionStorage.getItem("tokenKey");
    
    return token;
}

function getTokenWithHeader() {
    var token = sessionStorage.getItem("tokenKey");
    var headers = {};
    if (token) {
        headers.Authorization = 'Bearer ' + token;
    }

    return headers;
}

function removeToken() {
    sessionStorage.removeItem("tokenKey");
}
