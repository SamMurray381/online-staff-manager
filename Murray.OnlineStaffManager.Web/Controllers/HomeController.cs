﻿using Murray.OnlineStaffManager.Core;
using Murray.OnlineStaffManager.Core.Infrastructure;
using Murray.OnlineStaffManager.DAL.Repositories;
using Murray.OnlineStaffManager.Web.Models;
using Murray.OnlineStaffManager.Web.Models.ViewModels;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Murray.OnlineStaffManager.Web.Controllers
{
    public class HomeController : BaseController
    {
        private UserTable<AppMember> _appMemberRepository;
        public HomeController(UserTable<AppMember> appMemberRepository,
                              ICompanyRepository companyRepository) 
            : base(appMemberRepository, companyRepository)
        {
            _appMemberRepository = appMemberRepository;
        }

        public ActionResult Index()
        {
            return View();
        }
    }
}