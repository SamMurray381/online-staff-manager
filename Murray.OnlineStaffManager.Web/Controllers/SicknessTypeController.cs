﻿using System.Web.Mvc;
using Murray.OnlineStaffManager.DAL.Services;
using Murray.OnlineStaffManager.Web.Models.ViewModels;
using System.Collections.Generic;
using Murray.OnlineStaffManager.Models;
using AutoMapper;
using Murray.OnlineStaffManager.Web.Models;
using Murray.OnlineStaffManager.Core;
using Murray.OnlineStaffManager.DAL.Repositories;
using Murray.OnlineStaffManager.Core.Infrastructure;

namespace Murray.OnlineStaffManager.Web.Controllers
{
    [Authorize]
    public class SicknessTypeController : BaseController
    {
        private IEmployeeSicknessService _employeeSicknessService;
        public SicknessTypeController(UserTable<AppMember> appMemberRepository,
                                      IEmployeeSicknessService employeeSicknessService,
                                      ICompanyRepository companyRepository) 
            : base(appMemberRepository, companyRepository)
        {
            _employeeSicknessService = employeeSicknessService;
        }

        // GET: SicknessType
        public ActionResult List()
        {
            var model = Mapper.Map<IEnumerable<SicknessType>, IEnumerable<SicknessTypesListViewModel>>
                (_employeeSicknessService.GetAllSicknessTypes());
            return PartialView("_SicknessTypes", model);
        }
    }
}