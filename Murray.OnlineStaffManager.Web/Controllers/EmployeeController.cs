﻿using System.Web.Mvc;
using Murray.OnlineStaffManager.Models;
using System;
using AutoMapper;
using Murray.OnlineStaffManager.Web.Models.ViewModels;
using System.Collections.Generic;
using Murray.OnlineStaffManager.DAL.Services;
using Murray.OnlineStaffManager.Web.Models;
using Murray.OnlineStaffManager.Core;
using Murray.OnlineStaffManager.DAL.Repositories;
using Murray.OnlineStaffManager.Core.Infrastructure;

namespace Murray.OnlineStaffManager.Web.Controllers
{
    [Authorize]
    public class EmployeeController : BaseController
    {
        private IEmployeeService _employeeService;

        public EmployeeController(IEmployeeService employeeService,
                                  UserTable<AppMember> appMemberRepository,
                                  ICompanyRepository companyRepository) 
            : base(appMemberRepository, companyRepository)
        {
            _employeeService = employeeService;
        }

        public ActionResult AddEmployee(EmployeeAddViewModel employee)
        {
            if (!ModelState.IsValid)
            {
                return View("Add",employee);
            }

            var validEmployee = new Employee
            {
                EmployeeStartDate = employee.EmployeeStartDate,
                DateOfBirth = employee.Dob,
                Forename = employee.Forename,
                Surname = employee.Surname,
                CompanyId = (Guid)Session["CompanyId"]
            };

            _employeeService.Add(validEmployee);
            return RedirectToAction("Index", "Employee");
        }

        public ActionResult Add()
        {
            return View();
        }

        public ActionResult Index()
        {
            //todo: add pagination logic.
            var employees = Mapper.Map<IEnumerable<Employee> ,IEnumerable<ListEmployeesViewModel>>
                (_employeeService.GetAll((Guid)Session["CompanyId"]));

            return View(employees);
        }

        public ActionResult Edit(Guid employeeId)
        {
            var employee = Mapper.Map<Employee,EditEmployeeViewModel>(_employeeService.GetEmployeeDetails(employeeId));
            return View(employee);
        }

        public ActionResult DeleteEmployee(Guid employeeId)
        {
            _employeeService.DeleteById(employeeId);

            return RedirectToAction("Employees", "Employee");
        }

        public ActionResult CurrentEmployeesList()
        {
            var employees = Mapper.Map<IEnumerable<Employee>, IEnumerable<ListEmployeesViewModel>>
                (_employeeService.GetCurrentEmployees((Guid)Session["CompanyId"]));

            return PartialView("_CurrentEmployees", employees);
        }
        public ActionResult PastEmployeesList()
        {
            var employees = Mapper.Map<IEnumerable<Employee>, IEnumerable<ListEmployeesViewModel>>
                (_employeeService.GetPastEmployees((Guid)Session["CompanyId"]));

            return PartialView("_PastEmployees", employees);
        }

        public ActionResult Reactivate(Guid Id)
        {
            _employeeService.Reactivate(Id);

            return RedirectToAction("Index");
        }
    }
}