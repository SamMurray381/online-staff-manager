﻿using Murray.OnlineStaffManager.Core;
using Murray.OnlineStaffManager.Core.Infrastructure;
using Murray.OnlineStaffManager.DAL.Repositories;
using Murray.OnlineStaffManager.Models;
using System.Linq;
using System.Web.Mvc;

namespace Murray.OnlineStaffManager.Web.Controllers
{
    public class BaseController : Controller
    {
        private readonly UserTable<AppMember> _appMemberRepository;
        private ICompanyRepository _companyRepository;

        public BaseController(UserTable<AppMember> appMember, 
            ICompanyRepository companyRepository)
        {
            _appMemberRepository = appMember;
            _companyRepository = companyRepository;
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (User != null)
            {
                var username = User.Identity.Name;

                if (!string.IsNullOrEmpty(username))
                {
                    var user = _appMemberRepository.GetUserByName(username).First();
                    string fullName = "";
                    if(user != null)
                    {
                       fullName = string.Concat(new string[] { user.Forename, " ", user.Surname });
                    }
                    ViewData.Add("FullName", fullName);
                    Session.Add("CompanyId", user.CompanyId);
                    ViewData.Add("CompanyId", user.CompanyId);

                    // Store company name in ViewData
                    Company company = _companyRepository.GetById(user.CompanyId);
                    ViewData.Add("CompanyName", company.Name);
                }
            }
            base.OnActionExecuted(filterContext);
        }
    }
}