﻿using AutoMapper;
using Murray.OnlineStaffManager.Core;
using Murray.OnlineStaffManager.Core.Infrastructure;
using Murray.OnlineStaffManager.DAL.Repositories;
using Murray.OnlineStaffManager.DAL.Services;
using Murray.OnlineStaffManager.Models;
using Murray.OnlineStaffManager.Web.Models;
using Murray.OnlineStaffManager.Web.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
namespace Murray.OnlineStaffManager.Web.Controllers
{
    [Authorize]
    public class EmployeeSicknessController : BaseController
    {
        private IEmployeeSicknessService _employeeSicknessService;
        private IEmployeeService _employeeService;

        public EmployeeSicknessController(
            IEmployeeSicknessService employeeSicknessService,
            IEmployeeService employeeService,
            UserTable<AppMember> appMemberRepository,
            ICompanyRepository companyRepository)
            : base(appMemberRepository, companyRepository)
        {
            _employeeSicknessService = employeeSicknessService;
            _employeeService = employeeService;
        }

        public ActionResult Index()
        {
            //todo: add pagination logic
            var model = _employeeSicknessService.GetAll((Guid)Session["CompanyId"]);

            EmployeeSicknessListViewModel vm = new EmployeeSicknessListViewModel();

            foreach (var m in model)
            {
                // Historic
                if(m.SicknessEndDate < DateTime.Today)
                {
                    vm.Historic.Add(Mapper.Map<EmployeeSickness, EmployeeSicknessViewModel>(m));
                }
                // Current
                else
                {
                    vm.Current.Add(Mapper.Map<EmployeeSickness, EmployeeSicknessViewModel>(m));
                }
            }
            return View(vm);
        }

        public ActionResult Add()
        {
            // Get a list of all employees at the company 
            IEnumerable<Employee> employees = _employeeService.GetAll((Guid)Session["CompanyId"]);
            IEnumerable<SicknessType> sicknesses = _employeeSicknessService.GetAllSicknessTypes();

            EmployeeSicknessAddViewModel vm = new EmployeeSicknessAddViewModel();
            // Create a select list for employees
            foreach (var em in employees)
            {
                vm.EmployeeList.Add(new SelectListItem
                {
                    Text = em.FullName,
                    Value = em.Id.ToString()
                });
            }

            // Create a select list for sickness types
            foreach (var type in sicknesses)
            {
                vm.SicknessTypesList.Add(new SelectListItem
                {
                    Text = type.Description,
                    Value = type.Id.ToString()
                });
            }
            return View(vm);
        }

        public ActionResult AddSickness(EmployeeSicknessAddViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View("Add", model);
            }
            EmployeeSickness employeeSicknesses = 
                Mapper.Map<EmployeeSicknessAddViewModel, EmployeeSickness>
                  (model);

            // If the user has left the end date blank then null the value before persisting to db.
            if(model.SicknessEndDate == DateTime.MinValue)
            {
                employeeSicknesses.SicknessEndDate = null;
            }
            _employeeSicknessService.Add(employeeSicknesses);

            return RedirectToAction("Index");
        }

        public ActionResult Edit(Guid EmployeeSicknessId)
        {
            EmployeeSicknessDetailViewModel employeeSickness =
                Mapper.Map<EmployeeSickness, EmployeeSicknessDetailViewModel>
                (_employeeSicknessService.Get(EmployeeSicknessId));

            IEnumerable<SicknessType> sicknessTypes = _employeeSicknessService.GetAllSicknessTypes();
            foreach (var type in sicknessTypes)
            {
                employeeSickness.SicknessTypesList.Add(new SelectListItem
                {
                    Text = type.Description,
                    Value = type.Id.ToString()
                });
            }

            return View(employeeSickness);
        }

        public ActionResult NewSicknessType()
        {
            return View();
        }

        public ActionResult AddNewSicknessType(SicknessTypeAddViewModel model)
        {
            _employeeSicknessService.AddSicknessType(new SicknessType
            {
                CompanyId = (Guid)Session["CompanyId"],
                Description = model.Description,
                IsActive = true
            });

            return RedirectToAction("Index");
        }

        public ActionResult DeleteSicknessType(Guid SicknessTypeId)
        {
            _employeeSicknessService.DeleteSicknessTypeById(SicknessTypeId);
            return RedirectToAction("Index");
        }
    }
}