﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Murray.OnlineStaffManager.Web.Startup))]
namespace Murray.OnlineStaffManager.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
