﻿using System;

namespace Murray.OnlineStaffManager.Web.Models.ViewModels
{
    public class SicknessTypesListViewModel
    {
        public Guid Id { get;set; }
        public DateTime CreatedOn { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }
}