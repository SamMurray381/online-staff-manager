﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Murray.OnlineStaffManager.Web.Models.ViewModels
{
    public class EmployeeSicknessDetailViewModel
    {
        public EmployeeSicknessDetailViewModel()
        {
            SicknessTypesList = new List<SelectListItem>();
        }

        public Guid Id { get; set; }
        [Display(Name = "Sickness Type")]
        public Guid SicknessTypeId { get; set; }
        public List<SelectListItem> SicknessTypesList { get; set; }
        public string Forename { get; set; }
        public string Surname { get; set; }
        public string Comments { get; set; }
        [Required]
        public DateTime SicknessStartDate { get; set; }
        public DateTime? SicknessEndDate { get; set; }
    }
}