﻿using System.ComponentModel.DataAnnotations;

namespace Murray.OnlineStaffManager.Web.Models.ViewModels
{
    public class SicknessTypeAddViewModel
    {
        [Display(Name = "Sickness Name e.g. Flu")]
        [Required]
        public string Description { get; set; }
    }
}