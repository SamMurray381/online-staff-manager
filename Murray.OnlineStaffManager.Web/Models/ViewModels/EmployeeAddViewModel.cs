﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Murray.OnlineStaffManager.Web.Models.ViewModels
{
    public class EmployeeAddViewModel
    {
        /// <summary>
        /// Forename of the Employee.
        /// </summary>
        public string Forename { get; set; }
        /// <summary>
        /// Surname of the Employee.
        /// </summary>
        public string Surname { get; set; }
        /// <summary>
        /// Date of birth for the the Employee.
        /// </summary>
        [DisplayName("Date Of Birth")]
        [DataType(DataType.Date)]
        public DateTime Dob { get; set; }
        /// <summary>
        /// Day that the Employee began to work for the company.
        /// </summary>
        [DisplayName("Employee Start Date")]
        [DataType(DataType.Date)]
        public DateTime EmployeeStartDate { get; set; }

        // Other properties
    }
}