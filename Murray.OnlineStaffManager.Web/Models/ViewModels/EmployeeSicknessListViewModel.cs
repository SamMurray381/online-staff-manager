﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Murray.OnlineStaffManager.Web.Models.ViewModels
{
    public class EmployeeSicknessViewModel
    {
        public Guid Id { get; set; }
        public string Forename { get; set; }
        public string Surname { get; set; }
        public string SicknessType { get; set; }
        public string Comments { get; set; }
        public DateTime SicknessStartDate { get; set; }
        public DateTime? SicknessEndDate { get; set; }
    }
    public class EmployeeSicknessListViewModel
    {
       public EmployeeSicknessListViewModel()
       {
            Current = new List<EmployeeSicknessViewModel>();
            Historic = new List<EmployeeSicknessViewModel>();
       }
       public List<EmployeeSicknessViewModel> Current { get; set; }
       public List<EmployeeSicknessViewModel> Historic { get; set; }
    }
}