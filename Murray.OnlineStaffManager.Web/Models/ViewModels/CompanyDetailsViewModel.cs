﻿using System;

namespace Murray.OnlineStaffManager.Web.Models.ViewModels
{
    public class CompanyDetailsViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string WebsiteUrl { get; set; }
    }
}