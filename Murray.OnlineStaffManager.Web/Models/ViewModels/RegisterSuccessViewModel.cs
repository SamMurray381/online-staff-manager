﻿namespace Murray.OnlineStaffManager.Web.Models.ViewModels
{
    public class RegisterSuccessViewModel
    {
        public string Email { get; set; }
    }
}