﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Murray.OnlineStaffManager.Web.Models.ViewModels
{
    public class EditEmployeeViewModel
    {
        /// <summary>
        /// Unique global identifier of the Employee.
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// Forename of the Employee.
        /// </summary>
        public string Forename { get; set; }
        /// <summary>
        /// Surname of the Employee.
        /// </summary>
        public string Surname { get; set; }
        /// <summary>
        /// Day that the Employee began to work for the company.
        /// </summary>
        public DateTime EmployeeStartDate { get; set; }
        /// <summary>
        /// Date of birth for the the Employee.
        /// </summary>
        public DateTime DateOfBirth { get; set; }
    }
}