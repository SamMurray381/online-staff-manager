﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Murray.OnlineStaffManager.Web.Models.ViewModels
{
    public class EmployeeSicknessAddViewModel
    {
        public EmployeeSicknessAddViewModel()
        {
            this.SicknessTypesList = new List<SelectListItem>();
            this.EmployeeList = new List<SelectListItem>();
        }

        [Required]
        public Guid? EmployeeId { get; set; }
        [DisplayName("Employee")]
        public List<SelectListItem> EmployeeList { get; set; }
        [DisplayName("Sickness Type")]
        public List<SelectListItem> SicknessTypesList { get; set; }
        [Required]
        public Guid? SicknessTypeId { get; set; }
        public string Comments { get; set; }
        [Display(Name = "Sickness Start Date")]
        public DateTime SicknessStartDate { get; set; }
        [Display(Name = "Sickness End Date")]
        public DateTime? SicknessEndDate { get; set; }
    }
}