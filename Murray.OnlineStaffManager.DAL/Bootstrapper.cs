﻿using Murray.OnlineStaffManager.Core;
using Murray.OnlineStaffManager.DAL.Repositories;
using Murray.OnlineStaffManager.Models.Interfaces;
using SimpleInjector;
using SimpleInjector.Integration.WebApi;

namespace Murray.OnlineStaffManager.DAL
{
    public static class Bootstrapper
    {
        public static void Initialize()
        {
            var container = new Container();

            container.Options.DefaultScopedLifestyle = new WebApiRequestLifestyle();
            container.Register<IDbConnectionFactory, SqlConnectionFactory>(Lifestyle.Transient);
            container.Register<IEmployeeRepository, EmployeeRepository>(Lifestyle.Scoped);
        }
    }
}
