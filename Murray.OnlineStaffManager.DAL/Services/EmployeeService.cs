﻿using Murray.OnlineStaffManager.DAL.Repositories;
using Murray.OnlineStaffManager.Models;
using Murray.OnlineStaffManager.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Murray.OnlineStaffManager.DAL.Services
{
    public interface IEmployeeService : IService<Employee>
    {
        Employee GetEmployeeDetails(Guid employeeId);
        List<Employee> GetCurrentEmployees(Guid CompanyId);
        List<Employee> GetPastEmployees(Guid CompanyId);
        bool Reactivate(Guid id);
    }

    public class EmployeeService : IEmployeeService
    {
        private IEmployeeRepository _employeeRepository;

        public EmployeeService(IEmployeeRepository employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        public bool Add(Employee entity)
        {
            return _employeeRepository.Add(entity);
        }

        public bool Delete(Employee entity)
        {
            return _employeeRepository.Delete(entity);
        }

        public bool DeleteById(Guid id)
        {
            return _employeeRepository.DeleteById(id);
        }

        public Employee Get(Guid id)
        {
            return _employeeRepository.GetById(id);
        }

        public IEnumerable<Employee> GetAll(Guid CompanyId)
        {
            return _employeeRepository.All(CompanyId);
        }

        public List<Employee> GetCurrentEmployees(Guid CompanyId)
        {
            return _employeeRepository.All(CompanyId).Where(c => c.EmployeeEndDate == DateTime.MinValue)
                .ToList();
        }

        public Employee GetEmployeeDetails(Guid employeeId)
        {
            return _employeeRepository.GetEmployeeDetails(employeeId);
        }

        public List<Employee> GetPastEmployees(Guid CompanyId)
        {
            return _employeeRepository.All(CompanyId).Where(c => c.EmployeeEndDate != DateTime.MinValue)
                .ToList();
        }

        public bool Reactivate(Guid id)
        {
            return _employeeRepository.Reactivate(id);
        }

        public bool Update(Employee entity)
        {
            return _employeeRepository.Update(entity);
        }
    }
}
