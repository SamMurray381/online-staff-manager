﻿using Murray.OnlineStaffManager.DAL.Repositories;
using Murray.OnlineStaffManager.Models;
using Murray.OnlineStaffManager.Models.Interfaces;
using System;
using System.Collections.Generic;

namespace Murray.OnlineStaffManager.DAL.Services
{
    public interface IEmployeeSicknessService: IService<EmployeeSickness>
    {
        IEnumerable<SicknessType> GetAllSicknessTypes();
        bool AddSicknessType(SicknessType entity);
        bool DeleteSicknessTypeById(Guid id);
    }

    public class SicknessService : IEmployeeSicknessService
    {
        private IEmployeeSicknessRepository _sicknessRepository;

        public SicknessService(IEmployeeSicknessRepository sicknessRepository)
        {
            _sicknessRepository = sicknessRepository;
        }

        public bool Add(EmployeeSickness entity)
        {
            return _sicknessRepository.Add(entity);
        }

        public bool AddSicknessType(SicknessType entity)
        {
            return _sicknessRepository.AddSicknessType(entity);
        }

        public bool Delete(EmployeeSickness entity)
        {
            return _sicknessRepository.Delete(entity);
        }

        public bool DeleteById(Guid id)
        {
            return _sicknessRepository.DeleteById(id);
        }

        public bool DeleteSicknessTypeById(Guid id)
        {
            return _sicknessRepository.DeleteSicknessTypeById(id);
        }

        public EmployeeSickness Get(Guid id)
        {
            return _sicknessRepository.GetById(id);
        }

        public IEnumerable<EmployeeSickness> GetAll(Guid CompanyId)
        {
            return _sicknessRepository.All(CompanyId);
        }

        public IEnumerable<SicknessType> GetAllSicknessTypes()
        {
            return _sicknessRepository.GetAllSicknessTypes();
        }

        public bool Update(EmployeeSickness entity)
        {
            // Get the fields that the user cannot update
            var employeeSickness = 
                _sicknessRepository.GetById(entity.Id);

            // Bolt those fields on
            entity.CompanyId = employeeSickness.CompanyId;
            entity.EmployeeId = employeeSickness.EmployeeId;

            return _sicknessRepository.Update(entity);
        }
    }
}
