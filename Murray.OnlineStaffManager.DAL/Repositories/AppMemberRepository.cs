﻿using Dapper;
using Murray.OnlineStaffManager.Models;
using Murray.OnlineStaffManager.Models.Interfaces;
using System;
using System.Linq;

namespace Murray.OnlineStaffManager.DAL.Repositories
{
    public interface IAppMemberRepository : IRepository<AppUser>
    {
        AppUser GetByEmail(string email);
    }

    public class AppMemberRepository : IAppMemberRepository
    {
        protected IDbConnectionFactory _dbConnectionFactory { get; set; }

        public AppMemberRepository(IDbConnectionFactory dbConnectionFactory)
        {
            _dbConnectionFactory = dbConnectionFactory;
        }

        public bool Add(AppUser entity)
        {
            throw new NotImplementedException();
        }

        public IQueryable<AppUser> All(Guid CompanyId)
        {
            throw new NotImplementedException();
        }

        public bool Delete(AppUser entity)
        {
            throw new NotImplementedException();
        }

        public bool DeleteById(Guid id)
        {
            throw new NotImplementedException();
        }

        public AppUser GetById(Guid id)
        {
            throw new NotImplementedException();
        }

        public bool Update(AppUser entity)
        {
            bool isisSuccess = false;
            using (var dbConnection = _dbConnectionFactory.CreateConnection())
            {
                string query = string.Format(@"UPDATE Member SET Forename ='{0}', 
                                Surname = '{1}', Email = '{2}',
                                PhoneNumber = '{3}'
                                WHERE Id = '{4}'",
                                entity.Forename,
                                entity.Surname,
                                entity.Email,
                                entity.PhoneNumber,
                                entity.Id.ToString().ToUpper());
                dbConnection.Open();
                int rowsAffected = dbConnection.Execute(query);
                if (rowsAffected > 0)
                {
                    isisSuccess = true;
                }
                dbConnection.Close();
            }
            return isisSuccess;
        }

        public AppUser GetByEmail(string email)
        {
            AppUser user;
            using (var dbConnection = _dbConnectionFactory.CreateConnection())
            {
                dbConnection.Open();
                user = dbConnection.Query<AppUser>(
                    @"SELECT 
                        Id,Forename, Surname, CompanyId, Email, PhoneNumber 
                    FROM Member WHERE UserName = @email",
                       new { Email = email }).FirstOrDefault();
                dbConnection.Close();
            }
            return user;
        }
    }
}
