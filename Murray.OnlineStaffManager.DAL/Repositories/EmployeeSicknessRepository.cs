﻿using Dapper;
using Murray.OnlineStaffManager.Core.Helpers;
using Murray.OnlineStaffManager.Models;
using Murray.OnlineStaffManager.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Murray.OnlineStaffManager.DAL.Repositories
{
    public interface IEmployeeSicknessRepository : IRepository<EmployeeSickness>
    {
        IEnumerable<EmployeeSickness> GetAllSicknessesByEmployeeId(Guid employeeId);
        IEnumerable<SicknessType> GetAllSicknessTypes();
        bool AddSicknessType(SicknessType entity);
        bool DeleteSicknessTypeById(Guid id);
    }

    public class EmployeeSicknessRepository : IEmployeeSicknessRepository
    {
        protected IDbConnectionFactory _dbConnectionFactory { get; set; }

        public EmployeeSicknessRepository(IDbConnectionFactory dbConnectionFactory)
        {
            _dbConnectionFactory = dbConnectionFactory;
        }

        public bool Add(EmployeeSickness entity)
        {
            bool isSuccess = false;
            entity.Id = Guid.NewGuid();

            using (var dbConnection = _dbConnectionFactory.CreateConnection())
            {
                string sql = @"INSERT INTO EmployeeSickness(Id, EmployeeId, SicknessStartDate,
                               SicknessEndDate, SicknessTypeId, Comments) 
                               VALUES(@Id, @EmployeeId, @SicknessStartDate, 
                               @SicknessEndDate, @SicknessTypeId, @Comments);";
                dbConnection.Open();
                int rowsAffected = dbConnection.Execute(sql, entity);
                if(rowsAffected > 0)
                {
                    isSuccess = true;
                }
                dbConnection.Close();
            }
            return isSuccess;
        }

        public bool Delete(EmployeeSickness entity)
        {
            bool isSuccess = false;
            Guid employeeSicknessId = entity.Id;

            if (employeeSicknessId == Guid.Empty)
            {
                throw new Exception("Id has no value");
            }

            using (var dbConnection = _dbConnectionFactory.CreateConnection())
            {
                string sql = "UPDATE EmployeeSickness SET Deleted = 1 WHERE Id = @EmployeeSicknessId";
                dbConnection.Open();
                int affectedRows = dbConnection.Execute(sql, new
                {
                    @EmployeeSicknessId = employeeSicknessId
                });
                dbConnection.Close();

                if(affectedRows > 0)
                {
                    isSuccess = true;
                }
            }
            return isSuccess;
        }

        public bool DeleteById(Guid id)
        {
            bool isSuccess = false;
            if (id == Guid.Empty)
            {
                throw new Exception("Id has no value");
            }

            using (var dbConnection = _dbConnectionFactory.CreateConnection())
            {
                string sql = "UPDATE EmployeeSickness SET Deleted = 1 WHERE Id = @EmployeeSicknessId";
                dbConnection.Open();
                int rowsAffected = dbConnection.Execute(sql, new
                {
                    @EmployeeSicknessId = id
                });
                dbConnection.Close();
                if(rowsAffected > 0)
                {
                    isSuccess = true;
                }
            }
            return isSuccess;
        }

        public bool Update(EmployeeSickness entity)
        {
            bool isSuccess = false;
            using (var dbConnection = _dbConnectionFactory.CreateConnection())
            {
                string sql = string.Format(@"UPDATE 
                                EmployeeSickness SET 
                                SicknessStartDate =  @SicknessStartDate, 
                                SicknessEndDate = @SicknessEndDate,
                                SicknessTypeId = @SicknessTypeId, 
                                Comments = @Comments,  
                                Deleted = @Deleted
                                WHERE Id = '{0}'", entity.Id);
                dbConnection.Open();
                int rowsAffected = dbConnection.Execute(sql, entity);
                dbConnection.Close();

                if(rowsAffected > 0)
                {
                    isSuccess = true;
                }
            }
            return isSuccess;
        }

        public EmployeeSickness GetById(Guid id)
        {
            EmployeeSickness employeeSickness;
            using (var dbConnection = _dbConnectionFactory.CreateConnection())
            {
                var sql = @"SELECT EmployeeSickness.Id, Employee.Id AS EmployeeId, SicknessStartDate,
                            SicknessEndDate, SicknessTypeId, Forename, Surname, Comments 
                            FROM EmployeeSickness
                            INNER JOIN Employee
                            ON Employee.Id = EmployeeSickness.EmployeeId
                            WHERE EmployeeSickness.Id = @EmployeeSicknessId";
                dbConnection.Open();
                employeeSickness = dbConnection.Query<EmployeeSickness>(sql, new
                {
                    @EmployeeSicknessId = id
                }).SingleOrDefault();
                dbConnection.Close();
            }
            return employeeSickness;
        }

        public IQueryable<EmployeeSickness> All(Guid CompanyId)
        {
            IQueryable<EmployeeSickness> sicknessRecords;
            using (var dbConnection = _dbConnectionFactory.CreateConnection())
            {
                var sql = string.Format(@"SELECT 
	EmployeeSickness.Id As Id, 
	Employee.Id As EmployeeId, 
	Forename, 
	Surname,
    SicknessStartDate,
	SicknessEndDate,
	SicknessTypeId, 
	Comments 
FROM 
	EmployeeSickness
INNER JOIN 
	Employee
ON EmployeeSickness.EmployeeId = Employee.Id
WHERE Employee.CompanyId = '{0}'", CompanyId);
                dbConnection.Open();
                sicknessRecords = dbConnection.Query<EmployeeSickness>(sql).AsQueryable();
                dbConnection.Close();
            }
            return sicknessRecords;
        }

        public IEnumerable<EmployeeSickness> GetAllSicknessesByEmployeeId(Guid employeeId)
        {
            // This will use an EmployeeId to get a list of all their sicknesses
            IEnumerable<EmployeeSickness> sicknessRecords;
            using (var dbConnection = _dbConnectionFactory.CreateConnection())
            {
                var sql = @"SELECT Id, EmployeeId, SicknessStartDate,
                               SicknessEndDate, SicknessTypeId, Comments FROM EmployeeSickness
                               WHERE
                               EmployeeSickness.EmployeeId = @EmployeeId";
                dbConnection.Open();
                sicknessRecords = dbConnection.Query<EmployeeSickness>(sql, 
                    new
                    {
                        @EmployeeId = employeeId
                    });
                dbConnection.Close();
            }
            return sicknessRecords;
        }

        public IEnumerable<SicknessType> GetAllSicknessTypes()
        {
            // This will use an EmployeeId to get a list of all their sicknesses
            IEnumerable<SicknessType> sicknessTypes;
            using (var dbConnection = _dbConnectionFactory.CreateConnection())
            {
                var sql = @"SELECT * FROM SicknessType WHERE IsActive = 1";
                dbConnection.Open();
                sicknessTypes = dbConnection.Query<SicknessType>(sql).AsQueryable();
                dbConnection.Close();
            }
            return sicknessTypes;
        }

        public bool AddSicknessType(SicknessType entity)
        {
            bool isSuccess = false;
            // Create a new EmployeeId 
            entity.Id = Guid.NewGuid();

            using (var dbConnection = _dbConnectionFactory.CreateConnection())
            {
                string sql = string.Format(@"
                            INSERT INTO SicknessType(Id, Description, IsActive, CreatedOn, CompanyId) 
                            VALUES('{0}', '{1}', '{2}', '{3}', '{4}')",
                            entity.Id, 
                            entity.Description, 
                            entity.IsActive, 
                            DateConversion.ConvertUKDateToSqlDate(entity.CreatedOn),
                            entity.CompanyId);

                dbConnection.Open();
                int rowsAffected = dbConnection.Execute(sql);

                if (rowsAffected > 0)
                {
                    isSuccess = true;
                }

                dbConnection.Close();
            }

            return isSuccess;
        }

        public bool DeleteSicknessTypeById(Guid id)
        {
            bool isSuccess = false;
            if (id == Guid.Empty)
            {
                throw new Exception("Id has no value");
            }

            using (var dbConnection = _dbConnectionFactory.CreateConnection())
            {
                string sql = "UPDATE SicknessType SET IsActive = 0 WHERE Id = @SicknessTypeId";
                dbConnection.Open();
                int rowsAffected = dbConnection.Execute(sql, new
                {
                    @SicknessTypeId = id
                });
                dbConnection.Close();
                if (rowsAffected > 0)
                {
                    isSuccess = true;
                }
            }
            return isSuccess;
        }
    }
}
