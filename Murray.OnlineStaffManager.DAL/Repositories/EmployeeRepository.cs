﻿using Dapper;
using Murray.OnlineStaffManager.Models;
using System;
using System.Linq;
using Murray.OnlineStaffManager.Models.Interfaces;
using Murray.OnlineStaffManager.Core.Helpers;

namespace Murray.OnlineStaffManager.DAL.Repositories
{
    public interface IEmployeeRepository : IRepository<Employee>
    {
        Employee GetEmployeeDetails(Guid employeeId);
        bool Reactivate(Guid id);
    }

    public class EmployeeRepository : IEmployeeRepository
    {
        protected IDbConnectionFactory _dbConnectionFactory { get; set; }

        public EmployeeRepository(IDbConnectionFactory dbConnectionFactory)
        {
            _dbConnectionFactory = dbConnectionFactory;
        }
        public bool Add(Employee entity)
        {
            bool isSuccess = false;
            // Create a new EmployeeId 
            entity.Id = Guid.NewGuid();

            using (var dbConnection = _dbConnectionFactory.CreateConnection())
            {
                string sql = string.Format(@"INSERT INTO Employee(Id, Forename, Surname, DateOfBirth, 
                            EmployeeStartDate, CompanyId) 
                            VALUES('{0}', '{1}', '{2}', '{3}', '{4}', '{5}');", 
                            entity.Id,
                            entity.Forename,
                            entity.Surname,
                            DateConversion.ConvertUKDateToSqlDate(entity.DateOfBirth), 
                            DateConversion.ConvertUKDateToSqlDate(entity.EmployeeStartDate),
                            entity.CompanyId);

                dbConnection.Open();
                int rowsAffected = dbConnection.Execute(sql);

                if(rowsAffected > 0)
                {
                    isSuccess = true;
                }

                dbConnection.Close();
            }

            return isSuccess;
        }

        public IQueryable<Employee> All(Guid CompanyId)
        {
            IQueryable<Employee> employees;

            using (var dbConnection = _dbConnectionFactory.CreateConnection())
            {
                var sql = string.Format(@"SELECT 
                                *
                            FROM 
                                Employee
                            WHERE CompanyId = '{0}'
                            ORDER BY Surname ASC
                        ", CompanyId);
                dbConnection.Open();
                employees = dbConnection.Query<Employee>(sql).AsQueryable();
                dbConnection.Close();
            }
            return employees;
        }

        public bool DeleteById(Guid id)
        {
            bool isSuccess = false;
            if (id == Guid.Empty)
            {
                throw new Exception("EmployeeId has no value");
            }

            using (var dbConnection = _dbConnectionFactory.CreateConnection())
            {
                string sql = "UPDATE Employee SET EmployeeEndDate = GETDATE(), IsActive = 0 WHERE Id = @Id";
                dbConnection.Open();
                int rowsAffected = dbConnection.Execute(sql, new
                {
                    @Id = id
                });
                dbConnection.Close();

                if(rowsAffected > 0)
                {
                    isSuccess = true;
                }
            }

            return isSuccess;
        }

        public bool Delete(Employee entity)
        {
            throw new NotImplementedException();
        }

        public Employee GetById(Guid Id)
        {
            Employee employee;

            using (var dbConnection = _dbConnectionFactory.CreateConnection())
            {
                dbConnection.Open();
                employee = dbConnection.Query<Employee>(@"SELECT * FROM 
                                                                Employee WHERE Id =@EmployeeId",
                    new { EmployeeId = Id }).FirstOrDefault();
                dbConnection.Close();
            }
            return employee;
        }

        public bool Update(Employee entity)
        {
            bool isisSuccess = false;
            using (var dbConnection = _dbConnectionFactory.CreateConnection())
            {
                string query = string.Format(@"UPDATE Employee SET Forename ='{0}', 
                                Surname = '{1}', DateOfBirth = '{2}',
                                EmployeeStartDate = '{3}'
                                WHERE Id = '{4}'",
                                entity.Forename,
                                entity.Surname,
                                DateConversion.ConvertUKDateToSqlDate(entity.DateOfBirth),
                                DateConversion.ConvertUKDateToSqlDate(entity.EmployeeStartDate),
                                entity.Id.ToString().ToUpper());
                dbConnection.Open();
                int rowsAffected = dbConnection.Execute(query);
                if(rowsAffected > 0)
                {
                    isisSuccess = true;
                }
                dbConnection.Close();
            }
            return isisSuccess;
        }

        public Employee GetEmployeeDetails(Guid employeeId)
        {
            Employee employeeDetails;

            using (var dbConnection = _dbConnectionFactory.CreateConnection())
            {
                dbConnection.Open();
                employeeDetails = dbConnection.Query<Employee>(
                    @"SELECT 
	Employee.Id, 
	Forename,
	Surname,
	DateOfBirth, 
	EmployeeStartDate
FROM 
	Employee 
WHERE Employee.Id = @EmployeeId
",
                    new { EmployeeId = employeeId }).FirstOrDefault();
                dbConnection.Close();
            }

            return employeeDetails;
        }

        public bool Reactivate(Guid id)
        {
            bool isSuccess = false;
            if (id == Guid.Empty)
            {
                throw new Exception("Id has no value");
            }

            using (var dbConnection = _dbConnectionFactory.CreateConnection())
            {
                string sql = "UPDATE Employee SET EmployeeEndDate = null, IsActive = 1 WHERE Id = @Id";
                dbConnection.Open();
                int rowsAffected = dbConnection.Execute(sql, new
                {
                    @Id = id
                });
                dbConnection.Close();

                if (rowsAffected > 0)
                {
                    isSuccess = true;
                }
            }

            return isSuccess;
        }
    }
}
