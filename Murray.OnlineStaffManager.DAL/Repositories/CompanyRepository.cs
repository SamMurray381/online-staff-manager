﻿using Dapper;
using Murray.OnlineStaffManager.Models;
using Murray.OnlineStaffManager.Models.Interfaces;
using System;
using System.Linq;

namespace Murray.OnlineStaffManager.DAL.Repositories
{
    public interface ICompanyRepository : IRepository<Company>
    {
    }

    public class CompanyRepository : ICompanyRepository
    {
        protected IDbConnectionFactory _dbConnectionFactory;

        public CompanyRepository(IDbConnectionFactory dbConnectionFactory)
        {
            _dbConnectionFactory = dbConnectionFactory;
        }

        public bool Add(Company entity)
        {
            using (var dbConnection = _dbConnectionFactory.CreateConnection())
            {
                string sql = @"INSERT INTO Company(Id, Name, WebsiteUrl) 
                               VALUES(@Id, @Name, @WebsiteUrl)";

                dbConnection.Open();
                dbConnection.Execute(sql, entity);
                dbConnection.Close();
            }
            return true;
        }

        public IQueryable<Company> All(Guid CompanyId)
        {
            throw new NotImplementedException();
        }

        public bool Delete(Company entity)
        {
            throw new NotImplementedException();
        }

        public bool DeleteById(Guid id)
        {
            bool isSuccess = true;
            if (id == Guid.Empty)
            {
                throw new Exception("Id has no value");
            }

            using (var dbConnection = _dbConnectionFactory.CreateConnection())
            {
                string sql = "DELETE FROM Company WHERE Id = @Id";
                dbConnection.Open();
                int rowsAffected = dbConnection.Execute(sql, new
                {
                    @Id = id
                });
                dbConnection.Close();

                if (rowsAffected > 0)
                {
                    isSuccess = true;
                }
            }
            return isSuccess;
        }

        public Company GetById(Guid id)
        {
            Company company;
            if (id == Guid.Empty)
            {
                throw new Exception("Id has no value");
            }

            using (var dbConnection = _dbConnectionFactory.CreateConnection())
            {
                string sql = "SELECT * FROM Company WHERE Id = @Id";
                dbConnection.Open();
                company = dbConnection.Query<Company>(sql, new
                {
                    @Id = id
                }).FirstOrDefault();
                dbConnection.Close();
            }
            return company;
        }

        public bool Update(Company entity)
        {
            bool isisSuccess = false;
            using (var dbConnection = _dbConnectionFactory.CreateConnection())
            {
                string query = string.Format(@"UPDATE Company SET Name ='{0}', 
                                WebsiteUrl = '{1}'
                                WHERE Id = '{2}'",
                                entity.Name,
                                entity.WebsiteUrl,
                                entity.Id.ToString().ToUpper());
                dbConnection.Open();
                int rowsAffected = dbConnection.Execute(query);
                if (rowsAffected > 0)
                {
                    isisSuccess = true;
                }
                dbConnection.Close();
            }
            return isisSuccess;
        }
    }
}
